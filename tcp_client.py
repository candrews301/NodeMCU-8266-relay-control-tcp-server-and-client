import socket


#  Sets the target server you want to communicate with
target_host = "UpdateME"
target_port = 9999

#  Dictionary the devices attached to server
relays = [{"key": 1, "name": "Relay 1", "oncommand": b"R1ON", "offcommand": b"R1OFF"},
          {"key": 2, "name": "Relay 2", "oncommand": b"R2ON", "offcommand": b"R2OFF"}]


#  Function to send request to the server and receive the response
def send_request(target_host, target_port, request):
    #  AF_INET parameter is saying we are going to use a standard IPv4 address or hostname
    #  SOCK_STREAM indicates that the client will be a TCP client
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Connecting to the server
    client.connect((target_host, target_port))
    # Sending request to the client
    client.send(request)
    # Receiving the response
    response = str(client.recv(1024), "utf-8")
    # Closing the connection
    client.close()
    # Returning response
    return response


#  Main loop
while True:
    for r in relays:
        print(str(r["key"]) + ": ", r["name"])  # Prints name and key of devices for input selections
    choice = input("What Relay?: ")  # User input for relay choice
    state = input("ON or OFF?: ")  # User input for changing relay state
    for r in relays:
        try:
            if int(choice) == r["key"]:
                if state.lower() == "off":
                    request = r["offcommand"]
                    response = send_request(target_host, target_port, request)
                if state.lower() == "on":
                    request = r["oncommand"]
                    response = send_request(target_host, target_port, request)
        except Exception:
            response = "Exception Occurred: Did you enter a valid number?"
    print(response)
