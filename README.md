<h1>Socket Server and Client for MicroPython</h1><br>
Collection of scripts I wrote to control relays for home automation purposes using<br>
a nodeMCU and micropython. 
<p><h3>tcp_server.py</h3><br>
1. Update the bind_ip variable to the IP address of the node.<br>
2. Verify GPIO pins are correct<br>
3. Run tcp_server.py</p>
<p><h3>tcp_client.py</h3><br>
1. Update the target_host to match the servers IP address.<br>
2. Run script and follow onscreen instructions to trigger the relay on and off.</p>