import socket
import machine


a_led = machine.Pin(16, machine.Pin.OUT)  # Built in LED (pin: D0)
b_led = machine.Pin(2, machine.Pin.OUT)  # Built in LED (pin:D4)
relay_pin = machine.Pin(13, machine.Pin.OUT)  # Relay One (pin:D7)
relay_pin1 = machine.Pin(12, machine.Pin.OUT)  # Relay Two (pin:D6)

#  Internal IP binding for the socket connections
bind_ip = "UpdateME"  # Update this to your IP to accept incoming connections
bind_port = 9999
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # Instantiating the socket as server
server.bind((bind_ip, bind_port))  # Starting the server
server.listen(20)  # sets the number of backlog connections to 20


#  Client handling and response to verify the request was successful
def handle_client(client_socket):
    request = client_socket.recv(1024)  # Handles the request from the client.
    print("*** Received: %s" % request)  # Prints the request from the client
    client_socket.send(b"Success")  # sending a packet back
    client_socket.close()  # closes the TCP connection
    return request


#  Main loop
while True:
    client, addr = server.accept()
    request = handle_client(client)
    if request == b"R1ON":
        relay_pin1(0)  # Turn relay and LED on
        a_led.value(0)
    if request == b"R1OFF":
        relay_pin1(1)  # Turn relay and LED off
        a_led.value(1)
    if request == b"R2ON":
        relay_pin(0)
        b_led.value(0)
    if request == b"R2OFF":
        relay_pin(1) 
        b_led.value(1)

